<?php
/**
 * @copyright Copyright &copy; Malcolm Paul, Nitm Inc, 2014
 * @version 1.0.0
 */

namespace nitm\filemanager\widgets;

use Yii;
use nitm\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ListView;
use yii\bootstrap\ButtonGroup;
use yii\bootstrap\ButtonDropdown;
use kartik\widgets\FileInput;
use nitm\filemanager\models\Image;
use nitm\filemanager\helpers\Storage;
use nitm\helpers\Helper;
use nitm\helpers\Icon;
use nitm\filemanager\widgets\ImageUpload;
use nitm\helpers\WidgetHelper;

/**
 * Extends the kartik\widgets\FileInput widget.
 *
 * @author Malcolm Paul <lefteyecc@ninjasitm.com>
 * @since 1.0
 */
class Images extends BaseWidget
{

    /**
     * @var array the HTML attributes for the extra image wrapper.
     */
	public $containerOptions = [];

	public $actionsMode = 'buttons';
	
	public $makeDefaultText = 'Default';

	/**
	 * Callable that allows you to add extra markup afetr an image is rendered
	 *
	 * @var [type]
	 */
	public $afterImage;

	/**
	 * @var Options for the Upload UI
	 */
	public $uiOptions = [];

    /**
     * @var array the HTML attributes for the extra image wrapper.
     */
	public $infoOptions = [
		'class' => 'col-md-12 col-lg-12',
		'role' => 'infoContainer'
	];

	public $options = [
		'class' => 'col-md-12 col-lg-12',
		'role' => 'imagesContainer',
		'id' => 'images'
	];

	public $pluginOptions = [
		"pluginOptions" => [
			'previewFileType' => 'image',
			'showCaption' => false,
			'showPreview' => true,
			'showUpload' => true,
			'uploadClass' => 'btn btn-success image-upload-button',
			'removeClass' => 'btn btn-xs',
			'browseClass' => 'btn btn-xs'
		],
		'options' => [
			'accept' => 'image/*',
			'type' => 'file'
		]
	];

	public $pluginExtraOptions = [
		"pluginOptions" => [
			'previewFileType' => 'image',
			'showCaption' => false,
			'showPreview' => true,
			'showUpload' => true,
			'uploadClass' => 'btn btn-xs btn-success image-upload-button',
			'removeClass' => 'btn btn-xs',
			'browseClass' => 'btn btn-xs',
			'previewClass' => 'file-preview-sm'
		],
		'options' => [
			'accept' => 'image/*',
			'type' => 'file'
		]
	];

	protected $imageModel;

    /**
     * Runs the widget
     */
    public function run()
    {
		if(!isset($this->imageModel))
			$this->imageModel = $this->model instanceof Image ? $this->model : $this->model->image();
		$images = $this->getImages();
		$info = $this->getUploadUI();
		return Html::tag('div', $info.$images, $this->containerOptions);
    }

	protected function getAssets()
	{
		return [
			\nitm\filemanager\assets\ImageAsset::className()
		];
	}

	protected function getUploadUI()
	{
		return Html::tag('div',
			Html::tag('div', '<br>'.ImageUpload::widget(array_merge([
				'model' => $this->imageModel,
				'url' => '/image/save/'.$this->imageModel->remote_type.'/'.$this->imageModel->remote_id,
			], $this->uiOptions)), [
				"class" => "upload-images",
				"id" => "filemanagerUpload",
				'style' => 'display:block;'
			]), [
				'class' => 'col-sm-12',
		]);
	}

	protected function getInfoPane()
	{
		return Html::tag('div', Html::tag('div', $this->getUploadButton()), $this->infoOptions);
	}

	protected function getUploadButton()
	{
		$text = \yii\helpers\ArrayHelper::remove($this->buttonOptions, 'text', 'Add Images');
		$options = array_replace_recursive([
			'size' => 'large',
			'toggleButton' => [
				'tag' => 'a',
				'label' => Icon::forAction('plus')." ".$text,
				'href' => \Yii::$app->urlManager->createUrl(['/image/form/create/'.$this->model->isWhat().'/'.$this->model->getId(), '_format' => 'modal']),
				'title' => \Yii::t('yii', $text),
				'role' => 'dynamicAction createAction disabledOnClose',
				'class' => 'btn btn-primary'
			],
		], (array)$this->buttonOptions);

		return \nitm\widgets\modal\Modal::widget($options);
	}

	/**
	 * Get the preview image
	 * @param nitm\filemanager\models\Image $image
	 * @param boolean $default Is this the default image
	 * @param boolean $placeholder Is this a placeholder
	 * @return html string
	 */
	protected function getThumbnail($model=null, $default=false, $placeholder=false)
	{
		return \Yii::$app->getView()->render("/image/thumbnail", [
			'model' => $model,
			'wrapperOptions' => $thumbnailOptions
		]);
	}

	protected function getImages()
	{
		//Use smaller preview images for extra images
		$pluginOptions = $this->pluginOptions;
		$pluginOptions['pluginOptions']['previewClass'] = 'file-preview-sm';
		$this->options['data-id'] = $this->model->getId();
		return ListView::widget([
			'options' => $this->options,
			'dataProvider' => new \yii\data\ArrayDataProvider([
				'allModels' => $this->model->images
			]),
			'itemOptions' => [
				'tag' => false
			],
			'itemView' => function ($model, $key, $index, $widget) {
				$image =  $this->getImage($model);
				if(is_callable($this->afterImage)) {
					$image .= $this->afterImage($model, $key, $index, $widget);
				}
				return $image;
			}
		]);
	}

    /**
     * @var array the HTML attributes for the defualt image wrapper.
     */
    protected function defaultOptions()
	{
		return [
			'class' => 'text-center col-md-3 col-lg-3 col-sm-6',
			'role' => 'imageContainer'
		];
	}

	/**
	 * The default actions that are supported
	 */
	protected function defaultActions($model=null)
	{
		return [
			'info' => [
				'label' => Icon::show('info').Html::tag('span', ' Info', [
					'class' => 'hidden-sm'
				]),
				'options' => [
					'labeled' => true,
					'icon' => 'info',
					'class' => 'mini text-info toggle',
					'title' => \Yii::t('yii', 'Show more Information'),
					'data-pjax' => '0',
					'role' => "visibility",
					'data-id' => 'image-info'.$model->getId(),
					'href' => '#'
				]
			],
			'default' => [
				'tagName' => 'a',
				'label' => Icon::show('thumb-tack').Html::tag('span', $this->makeDefaultText, [
					'class' => 'hidden-sm'
				]),
				'options' => [
					'labeled' => true,
					'icon' => 'thumb-tack',
					'class' => 'mini text-success '.($model->isDefault() ? 'hidden' : ''),
					'title' => \Yii::t('yii', 'Set this image as default'),
					'data-pjax' => '0',
					'role' => "toggleDefaultImage",
					'data-id' => 'image-default'.$model->getId(),
					'data-parent' => 'image'.$model->getId(),
					'href' => $this->getActionUrl('default', $model)
				]
			],
			'get' => [
				'tagName' => 'a',
				'label' => Icon::show('download').Html::tag('span', ' Download', [
					'class' => 'hidden-sm'
				]),
				'options' => [
					'labeled' => true,
					'icon' => 'download',
					'class' => 'text-default mini',
					'title' => \Yii::t('yii', 'Download Image'),
					'data-pjax' => '0',
					'inline' => true,
					'data-parent' => 'image'.$model->getId(),
					'data-method' => 'get',
					'_target' => 'new',
					'href' => $model->url()
				]
			],
			'delete' => [
				'tagName' => 'a',
				'label' => Icon::show('remove').Html::tag('span', ' Delete', [
					'class' => 'hidden-sm'
				]),
				'options' => [
					'labeled' => true,
					'icon' => 'delete',
					'class' => 'text-danger mini',
					'title' => \Yii::t('yii', 'Delete Image'),
					'data-pjax' => '0',
					'role' => "deleteAction deleteImage metaAction",
					'data-parent' => '#image'.$model->getId(),
					'data-method' => 'post',
					'data-action' => 'delete',
					'data-url' => \Yii::$app->urlManager->createUrl([$this->getActionUrl('delete', $model), '_format' => 'json'])
				]
			]
		];
	}

	public function getImage($image)
	{
		$id = $this->getInputId($image);
		$this->pluginExtraOptions['attribute'] = 'images['.$id.']';
		$this->pluginExtraOptions['pluginOptions']['uploadUrl'] = '/image/save/'.$this->model->isWhat().'/'.$this->model->getId();
		$this->pluginExtraOptions['model'] = $this->model;
		$this->pluginExtraOptions['options']['id'] = $id;

		$defaultOptions = $this->defaultOptions();
        $defaultOptions['class'] .= ' image';
		$defaultOptions['id'] = $id;
		$defaultOptions['role'] .= ' '.($image->isDefault() ? 'defaultImage' : 'extraImage');

		return \Yii::$app->getView()->render("@nitm/filemanager/views/image/view", [
			'model' => $image,
			"actionsHtml" => $this->getActionHtml($image),
			'wrapperOptions' => $defaultOptions,
			'pluginOptions' => $this->pluginExtraOptions,
			'noBreadcrumbs' => true,
			'widget' => $this
		]);
	}

	public function getActionHtml($model) {
		if($this->actionsMode == 'buttons') {
			return $this->getButtons($model);
		} else {
			return $this->getButtonDropDown($model);
		}
	}

	public function getButtonDropDown($model) {
		return WidgetHelper::widget(ButtonDropdown::class, [
			'split' => true,
			'label' => Icon::show('bars').Html::tag('span', ' Options', [
				'class' => 'visible-lg'
			]),
			'encodeLabel' => false,
			'dropdown' => [
				'encodeLabels' => false,
				'items' => array_map(function ($action) {
					return [
						'encode' => false,
						'label' => $action['label'],
						'url' => ArrayHelper::getValue($action, 'options.href', ArrayHelper::getValue($action, 'options.data-url')),
						'linkOptions' => $action['options']
					];
				}, $this->getActions($model))
			],
		]);
	}

	public function getButtons($model) {
		return WidgetHelper::widget(ButtonGroup::class, [
			'encodeLabels' => false,
			'buttons' => $this->getActions($model)
		]);
	}

	public function getActionUrl($action, $model) {
		return '/'.$model->isWhat().'/'.$action.'/'.$model->getId();
	}

	protected function getProgressHtml()
	{
		return Html::tag('div',
			Html::tag('div',
				Html::tag('div', '', ['id' => 'percentage']),
				['id' => 'bar', 'class' => 'clear']
			).
			Html::tag('div', '', ['id' => 'message', 'class' => 'clear']),
			[
				'class' => 'progress well',
				'id' => 'progress',
				'style' => 'display:none'
			]
		);
	}
}
