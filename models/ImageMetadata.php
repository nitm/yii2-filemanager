<?php

namespace nitm\filemanager\models;

use Yii;

/**
 * This is the model class for table "images_metadata".
 *
 * @property int $image_id
 * @property string $key
 * @property string $value
 * @property string $created
 * @property string $updated
 * @property Images $image
 */
class ImageMetadata extends FileMetadata
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_images_metadata';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image_id', 'key_name', 'value'], 'required'],
            [['image_id', 'width', 'height', 'size'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['image_id', 'key_name'], 'unique', 'targetAttribute' => ['image_id', 'key_name'], 'message' => 'The combination of Image ID and Key has already been taken.'],
        ];
    }

    public function scenarios()
    {
        return array_merge(parent::behaviors(), [
            'default' => ['key_name', 'value', 'image_id', 'width', 'height', 'size'],
            'create' => ['key_name', 'value', 'image_id', 'width', 'height', 'size'],
            'update' => ['key_name', 'value', 'width', 'height', 'size'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'image_id' => Yii::t('app', 'Image ID'),
            'key_name' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(Images::className(), ['id' => 'image_id']);
    }

    /**
     * The link that signifies the metadata connection.
     *
     * @return array
     */
    public static function metadataLink()
    {
        return ['image_id' => 'id'];
    }
}
