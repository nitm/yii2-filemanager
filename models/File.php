<?php

namespace nitm\filemanager\models;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property int $author_id
 * @property string $url
 * @property string $thumbnail_url
 * @property string $file_name
 * @property int $remote_id
 * @property string $remote_type
 * @property string $type
 * @property string $title
 * @property int $size
 * @property int $width
 * @property int $height
 * @property string $date
 * @property string $date_gmt
 * @property string $update
 * @property string $update_gmt
 * @property FileTerms[] $fileTerms
 * @property User $user
 */
class File extends BaseFile
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nitm_files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['author_id', 'size'], 'integer'],
            [['date', 'date_gmt', 'update', 'update_gmt'], 'safe'],
            [['url', 'thumbnail_url', 'file_name', 'title'], 'string', 'max' => 555],
            [['type'], 'string', 'max' => 45],
            [['remote_type', 'remote_id', 'hash'], 'unique', 'targetAttribute' => ['remote_id', 'hash'], 'message' => 'This file already exists', 'on' => ['create']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'User ID',
            'url' => 'Location',
            'thumbnail_url' => 'Thumbnail Url',
            'file_name' => 'File Name',
            'type' => 'Type',
            'title' => 'Title',
            'size' => 'Size',
            'width' => 'Width',
            'height' => 'Height',
            'date' => 'Date',
            'date_gmt' => 'Date Gmt',
            'update' => 'Update',
            'update_gmt' => 'Update Gmt',
        ];
    }
}
