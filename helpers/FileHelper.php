<?php

namespace nitm\filemanager\helpers;

use yii\web\UploadedFile;
use yii\helpers\Inflector;
use yii\helpers\FileHelper as FileHelperBase;
use nitm\filemanager\models\File;
use nitm\helpers\ArrayHelper;
use nitm\helpers\Network;
use kartik\icons\Icon;
use nitm\filemanager\helpers\Storage;

/*
* This is the directory interface class that finds directory information and returns it
*/
class FileHelper extends FileHelperBase
{
    protected static $_helper;

    public static function writeFile($data, $path)
    {
        return static::getHelper()->save($data, $path);
    }

    public static function delete($path)
    {
        return static::getHelper()->delete($path);
    }

    public static function getHelper()
    {
        if (!isset(static::$_helper)) {
            static::$_helper = new \nitm\filemanager\helpers\storage\Local;
        }
        return static::$_helper;
    }

    public static function exists($directory)
    {
        $directory = static::resolveFile($directory);
        return is_dir($directory) && is_readable($directory);
    }

    public static function saveInternally($fileModel, $uploads, $options=[])
    {
        $ret_val = [];
        $saveOptions = [
            'method' => 'move'
        ];
        extract($options);
        $pathId = md5(is_null($id) ? uniqid() : $id);
        $name = File::getSafeName($name);
        //Increase the count for the files for this model $model
        $idx = ($count = $fileModel->getCount()->one()) != null ? $count->count() : 0;
        foreach ($uploads as $uploadedFile) {
            if ($uploadedFile->hasError) {
                $ret_val[] = new File();
                continue;
            }
            if ($uploadedFile->type == File::URL_MIME) {
                list($uploadedFile, $size) = UploadHelper::getFromUrl(file_get_contents($uploadedFile->tempName));
            } else {
                $size = getimagesize($uploadedFile->tempName);
            }

            //We're counting starting from 1
            $idx++;
            $file = new File(['scenario' => 'create']);
            $fileContentType = substr($uploadedFile->type, 0, strpos($uploadedFile->type, '/'));
            $directory = rtrim(implode(DIRECTORY_SEPARATOR, array_filter([rtrim(static::getDirectory($fileContentType), DIRECTORY_SEPARATOR), $name, $id])), DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR;
            $file->setAttributes([
                'width' => $size[0] || 0,
                'height' => $size[1] || 0,
                'type' => $uploadedFile->type,
                'file_name' => $uploadedFile->name,
                'remote_type' => $name,
                'remote_id' => $id,
                'slug' => File::getSafeName($name)."-$id-$fileContentType-$idx",
                'signature' => File::getHash($uploadedFile->tempName),
                'url' => $directory.implode('-', array_filter([Inflector::slug($uploadedFile->baseName), md5($uploadedFile->name)])).".".$uploadedFile->extension,
                'size' => $uploadedFile->size
            ], false);

            $originalPath = $file->url;
            $tempFile = new File([
                'url' => $uploadedFile->tempName,
                'type' => $uploadedFile->type
            ]);

            $existing = File::find()->where([
                "signature" => $file->signature,
                'remote_type' => $name,
                'remote_id' => $id
            ])->one();

            if ($existing instanceof File) {
                //If an file already exists for this file then swap files
                $file = $existing;
                \Yii::trace("Found dangling $name file ".$file->slug);
                $tempFile->id = $file->getId();
                // self::createThumbnails($file, $file->type, $file->getRealPath());
                $existing->remote_id = $id;
                $existing->save();
                $ret_val[] = $file;
            } else {
                if (!Storage::exists($file->url, $fileContentType) && $file->signature) {
                    //This file doesn't exist yet
                    $file->remote_id = $id;
                    $fileDir = dirname($file->getRealPath());

                    if (!Storage::containerExists($fileDir, $fileContentType)) {
                        Storage::createContainer($fileDir, true, [], $fileContentType);
                    }

                    $url = Storage::save($tempFile, $file->getRealPath(), [], false, $file->getRealPath(), $fileContentType);

                    if (filter_var($url, FILTER_VALIDATE_URL)) {
                        $proceed = true;
                        $file->url = $url;
                    } elseif (Storage::exists($file->getRealPath(), $fileContentType)) {
                        $proceed = true;
                    } else {
                        $proceed = false;
                    }

                    if ($proceed) {
                        if ($file->save()) {
                            \Yii::trace("Saved $fileContentType ".$file->slug);
                            /**
                             * Need top fix creating thumbnail sbefore uploading to AWS
                             */
                            $tempFile->setAttributes([
                                'id' => $file->getId(),
                                'remote_id' => $file->remote_id,
                                'remote_type' => $file->remote_type
                            ]);
                            $tempFile->setOldAttributes($file->getAttributes());
                            $tempFile->id = $file->id;
                            // self::createThumbnails($file, $file->type, $file->url);
                            $file->populateRelation('metadata', $tempFile->metadata);
                            $ret_val[] = $file;
                        } else {
                            \Yii::error("Unable to save file informaiton to database for ".$file->slug."\n".json_encode($file->getErrors()));
                        }
                    } else {
                        \Yii::trace("Unable to save physical file: ".$file->slug);
                    }
                } else {
                    //This file exists already lets attach it and update the thumbnails if necessary.
                    if ($file->save()) {
                        $ret_val[] = $file;
                        self::createThumbnails($file, $file->type, $file->url);
                    }
                }
            }
            if (!Network::isValidUrl($uploadedFile->tempName)) {
                unlink($uploadedFile->tempName);
            }
        }
        return $ret_val;
    }

    /*---------------------
        Protected Functions
    ---------------------*/

    /*
    * Get the proper directory
    * @param string $directory
    * @return string
    */
    protected static function resolveFile($directory)
    {
        $directory = ($directory[strlen($directory)-1] == DIRECTORY_SEPARATOR) ? $directory : $directory.DIRECTORY_SEPARATOR;
        return \Yii::getAlias($directory);
    }

    public static function getDirectory($fileContentType, $getAlias = false)
    {
        $dir = \Yii::$app->getModule('nitm-files')->getPath($fileContentType);
        if ($getAlias) {
            $dir = \Yii::getAlias($dir);
        }
        return $dir;
    }

    /*
     * Is this file an file?
     */
    protected static function isImage($file)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        return array_shift(explode('/', finfo_file($finfo, $file))) == $fileContentType;
    }
}
