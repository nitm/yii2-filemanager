<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use kartik\widgets\ActiveForm;
use kartik\tabs\TabsX;
use backend\widgets\Select2;
use nitm\helpers\Icon;
use nitm\filemanager\models\Image;

/* @var $this yii\web\View */
/* @var $model common\models\AppConfig */
/* @var $form yii\widgets\ActiveForm */
$key = 'image-upload';
$htmlId = Inflector::slug($key);
$htmlImageId = 'image-'.Inflector::slug($key);
?>
<?= Html::tag('div', \nitm\filemanager\widgets\ImageUpload::widget([
  "mode" => "single",
  "model" => $model,
  'widgetOptions' => [
      'clientEvents' => [
        'fileuploaddone' => 'function(e, data) {
          updateFileValues(data.result.files[0].url)
        }',
      ]
  ]
]), [
  'style' => 'display:block'
]) ?>
<br>
<div class="overlay-container thumbnail thumbnail-large" id="<?=$htmlImageId?>-wrapper" style="display: <?= $model->exists() ? 'block' : 'none';?>">
	<div class="overlay" style="position: absolute">
		<<?= Html::a(Icon::forAction('delete').'<br>Delete Image', '#', [
            'class' => 'fa-2x text-danger full-width text-center',
            'title' => \Yii::t('yii', 'Delete Image'),
            'data-pjax' => '0',
            'role' => 'deleteAction deleteImage metaAction',
            'data-parent' => '#image'.$model->getId(),
            'data-method' => 'post',
            'data-action' => 'delete',
            'data-url' => \Yii::$app->urlManager->createUrl([
                '/'.$model->isWhat().'/delete/'.$model->getId(),
                '_format' => 'json'
            ]),
            'data-after-callback' => "function (event, result) { clearImage(event)}"
        ]); ?>
	</div>
  <img id="<?= $htmlImageId ?>" src="<?= $model->url() ?>">
</div>

<script>
document.addEventListener("DOMContentLoaded", function() {
  updateFileValues('<?=$model->url()?>')
});
function updateFileValues(value) {
  if(value) {
    $("#<?=$htmlImageId?>").attr("src", value);
    $("#<?=$htmlImageId?>-wrapper").slideDown()
  } else {
    $("#<?=$htmlImageId?>-wrapper").slideUp()
  }
}

function clearImage(result) {
  $('#<?= $htmlImageId ?>').attr('src', null);
  $("#<?=$htmlImageId?>-wrapper").slideUp()
}
</script>
