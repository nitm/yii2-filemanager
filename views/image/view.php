<?php
use nitm\helpers\Html;
use yii\bootstrap\ButtonGroup;
use kartik\widgets\ActiveForm;
use kartik\widgets\ActiveField;
use kartik\grid\GridView;
use nitm\helpers\Icon;
use nitm\helpers\WidgetHelper;

/**
 * @var yii\web\View $this
 * @var provisioning\models\ProvisioningImage $model
 */

$options = isset($options) ? $options : [
	'id' => 'images',
	'role' => 'imagesContainer'
];

if(!isset($this->title))
	$this->title = $model->file_name;

if(!isset($noBreadcrumbs) || (isset($noBreadcrumbs) && !$noBreadcrumbs))
	echo \yii\widgets\Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]);

?>

<?php
	echo Html::beginTag('div', [
		"class" => 'well well-sm row media',
		'style' => 'overflow: visible',
		'role' => 'statusIndicator'.$model->getId().' imageContainer '.($model->isDefault() ? 'defaultImage' : 'extraImage'),
		'id' => 'image'.$model->getId()
	]);
?>
	<div class="col-md-4 col-lg-4 col-sm-7 text-center">
		<span class="media-middle">
			<?php
				if(\Yii::$app->user->identity->lastActive() < strtotime($model->created_at)
					|| \Yii::$app->user->identity->lastActive() < strtotime($model->updated_at))
					echo  \nitm\widgets\activityIndicator\ActivityIndicator::widget([
							'type' => 'create',
							'size' => 'large',
					]);
			?>
			<?= Html::a($model->icon->getIconHtml('small', [
				'class' => 'thumbnail thumbnail-lg media-object '.($model->isDefault() ? 'default' : ''),
				'url' => $model->url('small')
			]), $model->url('small')) ?>
		</span>
		<span class="media-middle">
			<?= Html::tag('strong', $model->file_name) ?>
		</span>
	</div>
	<div class="col-md-3 col-lg-2 col-sm-2 visible-lg text-center">
		<span class="media-middle">
			<?= $model->getSize(); ?>
		</span>
	</div>
	<div class="col-md-5 col-lg-5 col-sm-5 text-center">
		<span class="media-middle" style="padding-top: 1rem">
			<?= $actionsHtml ?>
		</span>
	</div>
	<div class="col-sm-12 hidden" id='image-info<?=$model->getId();?>'>
		<h2>Metadata Information</h2>
		<div class="well">
		<?php
			$metaInfo = \nitm\widgets\metadata\StatusInfo::widget([
			'items' => [
				[
					'blamable' => $model->author(),
					'date' => $model->created_at,
					'value' => $model->created_at,
					'label' => [
						'true' => "Created On ",
					]
				],
				[
					'value' => $model->type,
					'label' => [
						'true' => "Image type ",
					]
				],
				[
					'value' => $model->getSize(),
					'label' => [
						'true' => "Image size ",
					]
				],
			]
		]);


		$shortLink = \nitm\widgets\metadata\ShortLink::widget([
			'label' => 'Url',
			'url' => $model->url(),
			'header' => $model->file_name,
			'type' => (\Yii::$app->request->isAjax ? 'page' : 'modal'),
			'size' => 'large'
		]).
		\nitm\widgets\metadata\ShortLink::widget([
			'label' => 'Path',
			'url' => $model->getRealPath(),
			'header' => $model->file_name,
			'type' => (\Yii::$app->request->isAjax ? 'page' : 'modal'),
			'size' => 'large'
		]);
		echo Html::tag('tr',
			Html::tag('td', $metaInfo.$shortLink, [
				'colspan' => 10,
			]), [
			'class' => 'hidden',
			'id' => 'image-info'.$model->getId()
		]); ?>
		</div>
	</div>
<?= Html::endTag('div') ?>
